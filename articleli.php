<!DOCTYPE html>	
<head>
<title></title>

<!-- 社群連結fb/line -->
<!-- 這裡要套 -->
<meta property="og:url"  content="" />
<meta property="og:type" content="website" />
<meta property="og:site_name" content="" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:locale" content="zh_TW" />
<meta property="og:type" content="website" />
<!-- 抓banner圖 -->
<!-- 如果你分享文章的縮圖要是寬版的大圖的話，那你的圖片至少要大於 600 x 315 px
最大圖片大小不能超過 5MB,圖片的寬高最大不能超過 1500 x 1500 px-->
<meta property="og:image" content="images/ogimages.png" />
<meta property="og:image:type" content="image/png" /> 
<meta property="og:image:width" content="1500" />
<meta property="og:image:height" content="1500" />

<?php require('head.php') ?>


<!-- 這裡要套 -->
<!-- 
    "position": 1,指第一層、"name": "Books",指麵包屑名稱、"item": "https://example.com/books"，指網址
 -->
<!-- JSON-LD 結構化資料 --麵包屑導覽 -->
<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "BreadcrumbList",
    "itemListElement": [{
        "@type": "ListItem",
        "position": 1,
        "name": "Books",
        "item": "https://example.com/books"
    },{
        "@type": "ListItem",
        "position": 2,
        "name": "Science Fiction",
        "item": "https://example.com/books/sciencefiction"
    },{
        "@type": "ListItem",
        "position": 3,
        "name": "Award Winners"
    }]
}
</script>


<script language="javascript">
$(window).ready(function(){
	ScrollTrigger.matchMedia({
		// ipad 
		"(min-width: 768px)": function() {
	
		},
        "(min-width: 1440px)": function() {
	
		},
	  
		// mobile
		"(max-width: 767px)": function() {
	
		},
		  
		// all 
		"all": function() {;
            gsap.set(".js-patPageRedWhiteBg", {
                opacity: 1,
                zIndex: 10,
            });
		}
	}); 

});   
$(window).on('load',function(){
    ScrollTrigger.matchMedia({
		// ipad 
		"(min-width: 768px)": function() {
	
		},
        "(min-width: 1440px)": function() {
	
		},
	  
		// mobile
		"(max-width: 767px)": function() {
	
		},
		  
		// all 
		"all": function() {
            gsap.to(".js-patPageRedWhiteBg", {
				duration: 0.6,
				zIndex: -1,
				background: "#FFF",
				delay: 0,
                opacity: 0,
                ease: "Power3.easeInOut",
			});
		}
	}); 
});  
</script>

<body class="">

    <div class="patPageRedWhiteBg js-patPageRedWhiteBg"></div>
    <!-- loading.php拿掉，改logo.php -->
	<?php require('logo.php') ?>

	<!-- 手機視口導覽列 -->
	<?php require('smlHeader.php') ?>
	<!-- 電腦視口導覽列 -->
	<?php require('header.php') ?>

	
	<!-- 頁面內容 -->
	<div class="patPageContentWidth js-contentShow">
        <!-- banner區 -->
        <div class="max-width--1640">
            <div class="patPageBannerBk">
                <!-- 漫畫 -->
                <img src="images/pageBg.svg" alt="背景" class="patPageBanner-bg">
                <div class="patPageComic">
                    <img src="images/photo05.png" alt="人" class="patPageComic-img">
                    <p class="patPageComic-diaStyle patPageComic-diaStyle--style01">把網友詢問的案例<br />寫文章跟大家分享</p>
                    <p class="patPageComic-diaStyle patPageComic-diaStyle--style02">...以下省略3萬6千字</p>
                </div>
                
                <div class="patPageTitBiteBk pagArtli-biteBk">
                    <!-- 麵包屑 -->
                    <article class="eleBite mb-25">
                        <a href="index.php" class="eleBite-link">
                            首頁
                        </a>
                        <a href="articleli.php" class="eleBite-link">
                            保險知識文章
                        </a>
                        <a href="articleli.php" class="eleBite-link">
                            理賠案例
                        </a>
                    </article>
                    <div class="">
                        <h2 class="eleTitCh mb-5">理賠案例</h2>
                        <h2 class="eleTitEn">Articles classify</h2>
                    </div>
                </div>
            </div>
	    </div>

        <!-- 文章列表 -->
        <div class="max-width--1640">
            <div class="pagArtliListBk">

                <!-- 1頁13個 -->
                <!-- 文章 -->
                <!-- 第一個class:eleArticleList--latest -->
                <article class="eleArticleList eleArticleList--latest">
                    <img src="images/latest-article-label.svg" alt="label" class="pagArtliList--latestLabel js-pagArtliList--latestLabel">
                    <a href="articlein.php" class="eleArticleList-imgBk">
                        <div class="baseImgBk">
                            <img src="images/article01.png" alt="img" class="eleImgBk-img">
                        </div>
                    </a>
                    <div class="eleArticleList-textBk">
                        <div class="">
                            <a href="articleli.php" class="eleArticleList-label">理賠案例</a>
                        </div>
                        <a href="articlein.php" class="eleArticleList-textBk--tit">保險知識文章標題保保險知識文章標題保</a>
                        <p class="eleArticleList-textBk--date">2021.05.30</p>
                        <p class="eleArticleList-textBk--text">10多年來，我常在網路上替網友分析保單、保險理賠的各種情境10多年來，我常在網路上替網友分析保單、保險理賠的各種情境</p>
                    </div>
                </article>
                <article class="eleArticleList">
                    <a href="articlein.php" class="eleArticleList-imgBk">
                        <div class="baseImgBk">
                            <img src="images/no-image.svg" alt="img" class="eleImgBk-img">
                        </div>
                    </a>
                    <div class="eleArticleList-textBk">
                        <a href="articleli.php" class="eleArticleList-label">理賠案例</a>
                        <a href="articlein.php" class="eleArticleList-textBk--tit">保險知識文章標題保險知識文章標題保險知識文章標題保險知識文章標題保險知識文章標題保險知識文章標題</a>
                        <p class="eleArticleList-textBk--date">2021.05.30</p>
                        <p class="eleArticleList-textBk--text">10多年來，我常在網路上替網友分析保單、保險理賠的各種情境。您可能在論壇跟部落格都曾經看過我的文章或影片。</p>
                    </div>
                </article>
                <article class="eleArticleList">
                    <a href="articlein.php" class="eleArticleList-imgBk">
                        <div class="baseImgBk">
                            <img src="images/no-image.svg" alt="img" class="eleImgBk-img">
                        </div>
                    </a>
                    <div class="eleArticleList-textBk">
                        <a href="articleli.php" class="eleArticleList-label">理賠案例</a>
                        <a href="articlein.php" class="eleArticleList-textBk--tit">保險知識文章標題</a>
                        <p class="eleArticleList-textBk--date">2021.05.30</p>
                        <p class="eleArticleList-textBk--text"></p>
                    </div>
                </article>
                <article class="eleArticleList">
                    <a href="articlein.php" class="eleArticleList-imgBk">
                        <div class="baseImgBk">
                            <img src="images/no-image.svg" alt="img" class="eleImgBk-img">
                        </div>
                    </a>
                    <div class="eleArticleList-textBk">
                        <a href="articleli.php" class="eleArticleList-label">理賠案例</a>
                        <a href="articlein.php" class="eleArticleList-textBk--tit">保險知識文章標題保險知識文章標題保險知識文章標題保險知識文章標題</a>
                        <p class="eleArticleList-textBk--date">2021.05.30</p>
                        <p class="eleArticleList-textBk--text">10多年來，我常在網路上替網友分析保單、保險理賠的各種情境。您可能在論壇跟部落格都曾經看過我的文章或影片。</p>
                    </div>
                </article>
                <article class="eleArticleList">
                    <a href="articlein.php" class="eleArticleList-imgBk">
                        <div class="baseImgBk">
                            <img src="images/no-image.svg" alt="img" class="eleImgBk-img">
                        </div>
                    </a>
                    <div class="eleArticleList-textBk">
                        <a href="articleli.php" class="eleArticleList-label">理賠案例</a>
                        <a href="articlein.php" class="eleArticleList-textBk--tit">保險知識文章標題</a>
                        <p class="eleArticleList-textBk--date">2021.05.30</p>
                        <p class="eleArticleList-textBk--text">10多年來，我常在網路上替網友分析保單、保險理賠的各種情境</p>
                    </div>
                </article>
                <article class="eleArticleList">
                    <a href="articlein.php" class="eleArticleList-imgBk">
                        <div class="baseImgBk">
                            <img src="images/no-image.svg" alt="img" class="eleImgBk-img">
                        </div>
                    </a>
                    <div class="eleArticleList-textBk">
                        <a href="articleli.php" class="eleArticleList-label">理賠案例</a>
                        <a href="articlein.php" class="eleArticleList-textBk--tit">保險知識文章標題保險知識文章標題保險知識文章標題保險知識文章標題保險知識文章標題保險知識文章標題</a>
                        <p class="eleArticleList-textBk--date">2021.05.30</p>
                        <p class="eleArticleList-textBk--text">10多年來，我常在網路上替網友分析保單、保險理賠的各種情境。您可能在論壇跟部落格都曾經看過我的文章或影片。</p>
                    </div>
                </article>
                <article class="eleArticleList">
                    <a href="articlein.php" class="eleArticleList-imgBk">
                        <div class="baseImgBk">
                            <img src="images/no-image.svg" alt="img" class="eleImgBk-img">
                        </div>
                    </a>
                    <div class="eleArticleList-textBk">
                        <a href="articleli.php" class="eleArticleList-label">理賠案例</a>
                        <a href="articlein.php" class="eleArticleList-textBk--tit">保險知識文章標題</a>
                        <p class="eleArticleList-textBk--date">2021.05.30</p>
                        <p class="eleArticleList-textBk--text"></p>
                    </div>
                </article>
                <article class="eleArticleList">
                    <a href="articlein.php" class="eleArticleList-imgBk">
                        <div class="baseImgBk">
                            <img src="images/no-image.svg" alt="img" class="eleImgBk-img">
                        </div>
                    </a>
                    <div class="eleArticleList-textBk">
                        <a href="articleli.php" class="eleArticleList-label">理賠案例</a>
                        <a href="articlein.php" class="eleArticleList-textBk--tit">保險知識文章標題保險知識文章標題保險知識文章標題保險知識文章標題</a>
                        <p class="eleArticleList-textBk--date">2021.05.30</p>
                        <p class="eleArticleList-textBk--text">10多年來，我常在網路上替網友分析保單、保險理賠的各種情境。您可能在論壇跟部落格都曾經看過我的文章或影片。</p>
                    </div>
                </article>
                <article class="eleArticleList">
                    <a href="articlein.php" class="eleArticleList-imgBk">
                        <div class="baseImgBk">
                            <img src="images/no-image.svg" alt="img" class="eleImgBk-img">
                        </div>
                    </a>
                    <div class="eleArticleList-textBk">
                        <a href="articleli.php" class="eleArticleList-label">理賠案例</a>
                        <a href="articlein.php" class="eleArticleList-textBk--tit">保險知識文章標題</a>
                        <p class="eleArticleList-textBk--date">2021.05.30</p>
                        <p class="eleArticleList-textBk--text">10多年來，我常在網路上替網友分析保單、保險理賠的各種情境</p>
                    </div>
                </article>
                
            </div>
        </div>
    </div>

    <!-- 分頁區 -->
    <div class="eleSelPageBk">
        <a class="eleSelPageArrow eleSelPageArrow--left "></a>
        <a href="javascript:void(0);" title="第一頁" class="eleSelPageCount eleSelPageCount--pageIn mlr-3">1</a>
        <a href="javascript:void(0);" title="第一頁" class="eleSelPageCount mlr-3">2</a>
        <a href="javascript:void(0);" title="第一頁" class="eleSelPageCount mlr-3">3</a>
        <a href="javascript:void(0);" title="第一頁" class="eleSelPageCount mlr-3">4</a>
        <a href="javascript:void(0);" title="第一頁" class="eleSelPageCount mlr-3">5</a>
        <a class="eleSelPageArrow eleSelPageArrow--right"></a>
    </div>

	<?php require('footer.php') ?>
	<!-- line@按鈕 -->
	<?php require('lineBt.php') ?>	
	
</body>
</html>
