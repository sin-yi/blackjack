<!DOCTYPE html>	
<head>
<title></title>

<!-- 社群連結fb/line -->
<!-- 這裡要套 -->
<meta property="og:url"  content="" />
<meta property="og:type" content="website" />
<meta property="og:site_name" content="" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:locale" content="zh_TW" />
<meta property="og:type" content="website" />
<!-- 抓video封面圖 -->
<!-- 如果你分享文章的縮圖要是寬版的大圖的話，那你的圖片至少要大於 600 x 315 px
最大圖片大小不能超過 5MB,圖片的寬高最大不能超過 1500 x 1500 px-->
<!-- 這裡要套 -->
<meta property="og:image" content="images/article02.png" />
<meta property="og:image:type" content="image/png" /> 
<!-- YouTube 影片縮圖尺寸為 1280 x 720 像素，其最小寛度為 640 像素。該尺寸使用 16:9 的寬高比。將最大檔案大小保持在 2 MB 內。 -->
<meta property="og:image:width" content="1280" />
<meta property="og:image:height" content="720" />

<?php require('head.php') ?>

<!-- JSON-LD 結構化資料 -- 影片 -->
<script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "VideoObject",
      //影片名稱
      "name": "Introducing the self-driving bicycle in the Netherlands",
      //影片說明
      "description": "This spring, Google is introducing the self-driving bicycle in Amsterdam, the world's premier cycling city. The Dutch cycle more than any other nation in the world, almost 900 kilometres per year per person, amounting to over 15 billion kilometres annually. The self-driving bicycle enables safe navigation through the city for Amsterdam residents, and furthers Google's ambition to improve urban mobility with technology. Google Netherlands takes enormous pride in the fact that a Dutch team worked on this innovation that will have great impact in their home country.",
      //指向影片縮圖檔案的網址，可以只抓一個
      "thumbnailUrl": [
        "https://example.com/photos/1x1/photo.jpg",
        "https://example.com/photos/4x3/photo.jpg",
        "https://example.com/photos/16x9/photo.jpg"
       ],
       //首次發布日期
      "uploadDate": "2016-03-31T08:00:00+08:00",
    }
</script>

<script language="javascript">
$(window).ready(function(){
	ScrollTrigger.matchMedia({
		// ipad 
		"(min-width: 768px)": function() {
	
		},
        "(min-width: 1440px)": function() {
	
		},
	  
		// mobile
		"(max-width: 767px)": function() {
	
		},
		  
		// all 
		"all": function() {;
            gsap.set(".js-patPageRedWhiteBg", {
                opacity: 1,
                zIndex: 10,
            });
		}
	}); 

});   
$(window).on('load',function(){
    ScrollTrigger.matchMedia({
		// ipad 
		"(min-width: 768px)": function() {
	
		},
        "(min-width: 1440px)": function() {
	
		},
	  
		// mobile
		"(max-width: 767px)": function() {
	
		},
		  
		// all 
		"all": function() {
            gsap.to(".js-patPageRedWhiteBg", {
				duration: 0.6,
				zIndex: -1,
				background: "#FFF",
				delay: 0,
                opacity: 0,
                ease: "Power3.easeInOut",
			});
		}
	}); 
});  
</script>

<body class="">
    
    <div class="patPageRedWhiteBg js-patPageRedWhiteBg"></div>
    <div class="patLoadingBg "></div>
    <!-- loading.php拿掉，改logo.php -->
	<?php require('logo.php') ?>

	<!-- 手機視口導覽列 -->
	<?php require('smlHeader.php') ?>
	<!-- 電腦視口導覽列 -->
	<?php require('header.php') ?>

	
	<!-- 頁面內容 -->
	<div class="patPageContentWidth js-contentShow">
        <!-- banner區 -->
        <div class="max-width--1640">
            <div class="pagVideoinBannerBk">
                <!-- 文章標題資訊 -->
                <div class="pagArtinBanner-artInfo">
                    <a href="articleli.php" class="eleLabel pagArtinBanner-artInfo--label">理賠案例</a>
                    <h1 class="pagArtinBanner-artInfo--tit">保險公司的成本跟考核，是業務員的責任？</h1>
                    <p class="eleDate">2021.05.30</p>
                </div>
                <div class="pagVideoinBannerBk-VideoBk">
                    <div class="baseVideoBk">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/qFYxtntefoI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
                <!-- <div class="typo-textAlignCenter">
                    <img src="images/videImg.webp" alt="保險公司是溫室或是戰狼的舞台？" class="pagArtinBanner-Banner">
                </div> -->

                <div class="patPageTitBiteBk pagVideoinBannerBk-biteBk">
                    <!-- 麵包屑 -->
                    <article class="eleBite mb-25">
                        <a href="index.php" class="eleBite-link">
                            首頁
                        </a>
                        <a href="videoli.php" class="eleBite-link">
                            影音專區
                        </a>
                        <a href="videoli.php" class="eleBite-link">
                            理賠案例
                        </a>
                    </article>
                    <div class="">
                        <h2 class="eleTitCh mb-5">理賠案例</h2>
                        <h2 class="eleTitEn">Video classify</h2>
                    </div>
                </div>
            </div>
	    </div>

        <!-- 文章區 -->
        <div class="max-width--1640">
            <div class="pagVideoinTextBk">
                <div class="unreset textBk">
                    <!-- 文編放置區 -->
                    <p>
                        保險公司一張保單的成本，內容有什麼？傳統保險公司跟保險經紀公司（簡稱保經）兩種制度，對於保單成本的影響，會讓業務人員的分紅以及職涯產生何種差異？這篇文章能讓想要了解從事保險業務的人，分析自己的需求，選擇適合自己條件的制度。
                    </p>
                    <div class="clear"></div>
                </div>
            </div>
        </div>

        <?php require('footer.php') ?>
        <!-- line@按鈕 -->
        <?php require('lineBt.php') ?>	
       
    </div>


	
</body>
</html>
