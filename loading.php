<script language="javascript">
//loading特效
$(window).ready(function(){
	//loading動畫
	gsap.to(".js-loadingLogo", {
		opacity: 1,
		duration: 0.4,
		ease: "Power3.easeInOut",
	});
	gsap.to(".js-loadingele01", {
		scaleX: 1,
		scaleY: 1,
		opacity: 1,
		transformOrigin: "0% 100%",
		delay: 1,
		duration: 0.4,
        ease: "Bounce.easeOut",
	});     
});
$(window).on('load',function(){
	ScrollTrigger.matchMedia({
		// ipad 
		"(min-width: 768px)": function() {
			//loading 動畫
            var tl01 = gsap.timeline();
			tl01.to(".js-patLoadingBg", {
				duration: 0.6,
				zIndex: 1,
				ease: "Power3.easeInOut",
			});
			tl01.to(".js-loadingele01", {
				scaleX: 2,
				scaleY: 2,
				opacity: 0,
				transformOrigin: "0% 100%",
				delay: 1,
				duration: 0.5,
				zIndex: 1,
				ease: "expo.out",
			});
			tl01.to(".js-loadingLogo", {
				duration: 0.6,
				delay: 0,
				width: "180px",
				top: "30px",
				left: "30px",
				// ease: Bounce.easeOut,
				// ease: {ease: Power3.easeInOut, y: 0 },
				// ease: "elastic.out(1,0.3)",
				// ease: "back.out(1.7)",
				ease: "power4.out",
			});
			tl01.to(".jsNavSml", {
				duration: 0.5,
				opacity: "1",
				ease: {ease: Power3.easeInOut, y: 0 },
			});
		},
        "(min-width: 1440px)": function() {
			//loading 動畫
            var tl01 = gsap.timeline();
			tl01.to(".js-patLoadingBg", {
				duration: 0.6,
				zIndex: 1,
				ease: "Power3.easeInOut",
			});
			tl01.to(".js-loadingele01", {
				scaleX: 2,
				scaleY: 2,
				opacity: 0,
				transformOrigin: "0% 100%",
				delay: 1,
				duration: 0.5,
				ease: "expo.out",
			});
			tl01.to(".js-loadingLogo", {
				duration: 0.6,
				delay: 0,
				width: "220px",
				top: "40px",
				left: "60px",
				ease: "power4.out",
			});
		},
		"(min-width: 1856px)": function() {
			//loading 動畫
            var tl01 = gsap.timeline();
			tl01.to(".js-patLoadingBg", {
				duration: 0.6,
				zIndex: 1,
				ease: "Power3.easeInOut",
			});
			tl01.to(".js-loadingele01", {
				scaleX: 2,
				scaleY: 2,
				opacity: 0,
				transformOrigin: "0% 100%",
				delay: 1,
				duration: 0.5,
				ease: "expo.out",
			});
			tl01.to(".js-loadingLogo", {
				duration: 0.6,
				delay: 0,
				width: "220px",
				top: "40px",
				left: "130px",
				ease: "power4.out",
			});
		},
	  
		// mobile
		"(max-width: 767px)": function() {
			//loading 動畫
			var tl01 = gsap.timeline();
			tl01.to(".js-patLoadingBg", {
				duration: 0.6,
				zIndex: 1,
				ease: "Power3.easeInOut",
			});
			tl01.to(".js-loadingele01", {
				scaleX: 3,
				scaleY: 3,
				opacity: 0,
				transformOrigin: "0% 100%",
				delay: 0.4,
				duration: 0.5,
				// ease: "expo.out",
				// ease: "Power3.easeInOut",
			});
			tl01.to(".js-loadingLogo", {
				duration: 0.6,
				delay: 0,
				width: "110px",
				top: "15px",
				left: "20px",
				ease: "power4.out",
			});
		},
		  
		// all 
		"all": function() {

			
		}		  
	}); 

});
</script>

<!-- 首頁loading動畫 -->
<a href="index.php" title="保險黑傑克首頁" class="patLoadingBg-logoLink">
	<img src="images/logo.svg" alt="logo" class="patLoadingBg-logo js-loadingLogo">
</a>
<img src="images/loadingele01.svg" alt="logo" class="patLoadingBg-ele01 js-loadingele01">
<div class="patLoadingBg js-patLoadingBg"></div>
<!-- <div class="patLoadingBg js-patLoadingBg">
	<img src="images/bannerBg.svg" alt="" class="patLoadingBg-ele" mode="widthFix">
</div> -->
