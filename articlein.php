<!DOCTYPE html>	
<head>
<title></title>

<!-- 社群連結fb/line -->
<!-- 這裡要套 -->
<meta property="og:url"  content="" />
<meta property="og:type" content="website" />
<meta property="og:site_name" content="" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:locale" content="zh_TW" />
<meta property="og:type" content="website" />
<!-- 抓banner圖 -->
<!-- 如果你分享文章的縮圖要是寬版的大圖的話，那你的圖片至少要大於 600 x 315 px
最大圖片大小不能超過 5MB,圖片的寬高最大不能超過 1500 x 1500 px-->
<!-- 這裡要套 -->
<meta property="og:image" content="images/article02.png" />
<meta property="og:image:type" content="image/png" /> 
<meta property="og:image:width" content="1200" />
<meta property="og:image:height" content="630" />

<?php require('head.php') ?>

<!-- JSON-LD 結構化資料 --文章 -->
<script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "BlogPosting",
      //文章的標題。標題不得超過 110 個字元。
      "headline": "Article headline",
      //圖片可以只抓一張
      "image": [
        "https://example.com/photos/1x1/photo.jpg",
        "https://example.com/photos/4x3/photo.jpg",
        "https://example.com/photos/16x9/photo.jpg"
       ],
       //出版時間
      "datePublished": "2015-02-05T08:00:00+08:00",
    }
</script>

<script language="javascript">
$(window).ready(function(){
	ScrollTrigger.matchMedia({
		// ipad 
		"(min-width: 768px)": function() {
	
		},
        "(min-width: 1440px)": function() {
	
		},
	  
		// mobile
		"(max-width: 767px)": function() {
	
		},
		  
		// all 
		"all": function() {;
            gsap.set(".js-patPageRedWhiteBg", {
                opacity: 1,
                zIndex: 10,
            });
		}
	}); 

});   
$(window).on('load',function(){
    ScrollTrigger.matchMedia({
		// ipad 
		"(min-width: 768px)": function() {
	
		},
        "(min-width: 1440px)": function() {
	
		},
	  
		// mobile
		"(max-width: 767px)": function() {
	
		},
		  
		// all 
		"all": function() {
            gsap.to(".js-patPageRedWhiteBg", {
				duration: 0.6,
				zIndex: -1,
				background: "#FFF",
				delay: 0,
                opacity: 0,
                ease: "Power3.easeInOut",
			});
		}
	}); 
});  
</script>

<body class="">

    <div class="patPageRedWhiteBg js-patPageRedWhiteBg"></div>
    <div class="patLoadingBg "></div>
    <!-- loading.php拿掉，改logo.php -->
	<?php require('logo.php') ?>

	<!-- 手機視口導覽列 -->
	<?php require('smlHeader.php') ?>
	<!-- 電腦視口導覽列 -->
	<?php require('header.php') ?>

	
	<!-- 頁面內容 -->
	<div class="patPageContentWidth js-contentShow">
	<!-- <div class="patPageContentWidth"> -->
        <!-- banner區 -->
        <div class="max-width--1640">
            <div class="pagArtinBannerBk">
                <!-- 文章標題資訊 -->
                <div class="pagArtinBanner-artInfo">
                    <a href="articleli.php" class="eleLabel pagArtinBanner-artInfo--label">理賠案例</a>
                    <h1 class="pagArtinBanner-artInfo--tit">保險公司是溫室或是戰狼的舞台？</h1>
                    <p class="eleDate">2021.05.30</p>
                </div>

                <div class="typo-textAlignCenter">
                    <img src="images/article01.png" alt="保險公司是溫室或是戰狼的舞台？" class="pagArtinBanner-Banner">
                </div>

                <div class="patPageTitBiteBk pagArtinBannerBk-biteBk">
                    <!-- 麵包屑 -->
                    <article class="eleBite mb-25">
                        <a href="index.php" class="eleBite-link">
                            首頁
                        </a>
                        <a href="articleli.php" class="eleBite-link">
                            保險知識文章
                        </a>
                        <a href="articleli.php" class="eleBite-link">
                            理賠案例
                        </a>
                    </article>
                    <div class="">
                        <h2 class="eleTitCh mb-5">理賠案例</h2>
                        <h2 class="eleTitEn">Articles classify</h2>
                    </div>
                </div>
            </div>
	    </div>

        <!-- 文章區 -->
        <div class="max-width--1640">
            <div class="pagArtinTextBk">
                <div class="unreset textBk">
                    <!-- 文編放置區 -->
                    <!-- <h1>
                        h1保險公司一張保單的成本，內容有什麼？傳統保險公司跟保險經紀公司（簡稱
                        保經）兩種制度，對於保單成本的影響，會讓業務人員的分紅以及職涯產生何
                        種差異？這篇文章能讓想要了解從事保險業務的人，分析自己的需求，選擇適
                        合自己條件的制度。
                    </h1>
                    <h2>
                        h2保險公司一張保單的成本，內容有什麼？傳統保險公司跟保險經紀公司（簡稱
                        保經）兩種制度，對於保單成本的影響，會讓業務人員的分紅以及職涯產生何
                        種差異？這篇文章能讓想要了解從事保險業務的人，分析自己的需求，選擇適
                        合自己條件的制度。
                    </h2>
                    <h3>
                        h3保險公司一張保單的成本，內容有什麼？傳統保險公司跟保險經紀公司（簡稱
                        保經）兩種制度，對於保單成本的影響，會讓業務人員的分紅以及職涯產生何
                        種差異？這篇文章能讓想要了解從事保險業務的人，分析自己的需求，選擇適
                        合自己條件的制度。
                    </h3>
                    <h4>
                        h4保險公司一張保單的成本，內容有什麼？傳統保險公司跟保險經紀公司（簡稱
                        保經）兩種制度，對於保單成本的影響，會讓業務人員的分紅以及職涯產生何
                        種差異？這篇文章能讓想要了解從事保險業務的人，分析自己的需求，選擇適
                        合自己條件的制度。
                    </h4>
                    <h5>
                        h5保險公司一張保單的成本，內容有什麼？傳統保險公司跟保險經紀公司（簡稱
                        保經）兩種制度，對於保單成本的影響，會讓業務人員的分紅以及職涯產生何
                        種差異？這篇文章能讓想要了解從事保險業務的人，分析自己的需求，選擇適
                        合自己條件的制度。
                    </h5>
                    <h6>
                        h6保險公司一張保單的成本，內容有什麼？傳統保險公司跟保險經紀公司（簡稱
                        保經）兩種制度，對於保單成本的影響，會讓業務人員的分紅以及職涯產生何
                        種差異？這篇文章能讓想要了解從事保險業務的人，分析自己的需求，選擇適
                        合自己條件的制度。
                    </h6>
                    <p>
                        p保險公司一張保單的成本，內容有什麼？傳統保險公司跟保險經紀公司（簡稱保經）兩種制度，對於保單成本的影響，會讓業務人員的分紅以及職涯產生何種差異？這篇文章能讓想要了解從事保險業務的人，分析自己的需求，選擇適合自己條件的制度。
                    </p> -->
                    <!-- 摘要 -->
                    <div class="sumBk">
                        保險公司一張保單的成本，內容有什麼？傳統保險公司跟保險經紀公司（簡稱保經）兩種制度，對於保單成本的影響，會讓業務人員的分紅以及職涯產生何種差異？這篇文章能讓想要了解從事保險業務的人，分析自己的需求，選擇適合自己條件的制度。
                    </div>
                    <p>
                        我剛要從保險公司離開到保經公司的時候，就聽到保經公司的人說：你看保險公司的考核是多麼不人道，有分季考核或半年考核，月有陰晴圓缺，
                        但是他卻要求你永遠有業績，這怎麼可能? 沒業績的就只好自己買業績。
                    </p>
                    <p>
                        但保險公司的同事卻是勸我說：你看那些會去保經的就是業績不好，才被考核掉的。或是說：
                        人是有惰性的，沒有考核，就沒有業績。
                    </p>
                    <p>
                        上面兩個說法，到底誰對誰錯呢？
                    </p>
                    <!-- 段落標題 -->
                    <div class="parHeadingBk">
                        以壽險公司長年期保單的成本分析為例，分析一張保單的成本：
                    </div>
                    <p>
                        保險公司製作一張保單主要有三個成本分類：製單成本、營業費用、附加費用。
                    </p>
                    <p>
                        １．製單成本：公司固定人事成本、核保、理賠、將來要理賠給客戶的錢--保單的責任準備金。<br />
                        ２．營業費用：包含廣告、業務員的傭金、辦公室租金、助理、水電。<br />
                        ３．附加費用：業務員的勞健保退休金、年終獎金、教育訓練...。
                    </p>
                    <img src="images/textImg01.png" alt="">
                    <p>
                        保險公司今天請個業務員賣保單，有沒有給你薪水？如果有，而且要求你要打卡報到，就是標準的僱傭制。 <br />
                        如果沒有給薪水，而且沒要求考核，你的收入全靠你的業績獎金，就是承攬制。不過大部分保險公司都沒有底薪，
                        但是要考核。業績有發傭金還有組織津貼的，屬於混合制。上述這一段話聽不懂、看不懂也沒關係，因為老闆搞的清楚就好了。
                    </p>
                    <!-- 段落標題 -->
                    <div class="parHeadingBk">
                        為什麼保險公司需要考核？考核實際上有用嗎？<br />
                        保險公司的考核，大概是分成： <br />
                        １．新人考核　２．主管考核　３．處經理以上有另外一套的考核
                    </div>
                    <p>
                        等一下！是保險公司養這些同仁還是業務員養的？<br />
                        講這麼多的廢話，跟業務員有關係嗎？<br />
                        當然有！
                    </p>
                    <p>
                        很多經理為了自己底下的業務員不要被砍，影響公司對經理的考核，通常不願意淘汰考核
                        不過的業務員。如此一來考核的意義就打折扣了，於是保險公司就無端的要幫產值低的同
                        仁交勞健保跟退休金。
                    </p>
                    <p>
                        根據保險公司財報分析，保險公司賣出一張保單，假設業務員傭金100圓，如果保險公司
                        今天自己養業務員，每發出100圓佣金給業務員，加上業務的勞健保費用約25圓、還要負
                        擔其他人事成本...保險公司付出的真正成本大約是145~155圓。大約為傭金的1.5倍。
                    </p>
                    <img src="images/textImg02.png" alt="">
                    <div class="imgHeading">保險公司給業務的成本為大約為實際傭金的1.5倍</div>
                    <img src="images/textImg03.png" alt="資料來源：現代保險雜誌2020，5月號" title="資料來源：現代保險雜誌2020，5月號">
                    <div class="imgHeading">資料來源：現代保險雜誌2020，5月號</div>
                    <!-- 段落標題 -->
                    <div class="parHeadingBk">
                        結論
                    </div>
                    <div class="conBk">
                        看到這裡，你會發現身旁有很多溫室裡的花朵，
                        正在靠你貢獻勞健保…你也會發現為什麼你會做了5年、8年永遠在主任、襄理、
                        經理、業務員之間一直六道輪迴。
                    </div>
                    <div class="conBk--02">
                        結論：
                        如果你是戰將，應該考慮抽成制度是否對你比較有利。 <br />
                        下一集將分析保險經紀公司的獎金結構。
                    </div>
                    <h2>相關文章</h2>
                    <a href="javascript:void(0);" class="">子宮頸抹片檢查發炎未告知，被保險公司解除契約？搞懂這個關鍵，成功救回保單！</a>
                    <a href="javascript:void(0);" class="">子宮頸抹片檢查發炎未告知，被保險公司解除契約？搞懂這個關鍵，成功救回保單！</a>

                    <!-- 表格 -->
                    <style>
                        /* info (hed, dek, source, credit) */
                    .rg-container {
                    font-family: 'Lato', Helvetica, Arial, sans-serif;
                    font-size: 16px;
                    line-height: 1.4;
                    margin: 0;
                    padding: 1em 0.5em;
                    color: #222;
                    }
                    .rg-header {
                    margin-bottom: 1em;
                    text-align: left;
                    }

                    .rg-header > * {
                    display: block;
                    }
                    .rg-hed {
                    font-weight: bold;
                    font-size: 1.4em;
                    }
                    .rg-dek {
                    font-size: 1em;
                    }

                    .rg-source {
                    margin: 0;
                    font-size: 0.75em;
                    text-align: right;
                    }
                    .rg-source .pre-colon {
                    text-transform: uppercase;
                    }

                    .rg-source .post-colon {
                    font-weight: bold;
                    }

                    /* table */
                    table.rg-table {
                    width: 100%;
                    margin-bottom: 0.5em;
                    font-size: 1em;
                    border-collapse: collapse;
                    border-spacing: 0;
                    }
                    table.rg-table tr {
                    -moz-box-sizing: border-box;
                    box-sizing: border-box;
                    margin: 0;
                    padding: 0;
                    border: 0;
                    font-size: 100%;
                    font: inherit;
                    vertical-align: baseline;
                    text-align: left;
                    color: #333;
                    }
                    table.rg-table thead {
                    border-bottom: 3px solid #ddd;
                    }
                    table.rg-table tr {
                    border-bottom: 1px solid #ddd;
                    color: #222;
                    }
                    table.rg-table tr.highlight {
                    background-color: #dcf1f0 !important;
                    }
                    table.rg-table.zebra tr:nth-child(even) {
                    background-color: #f6f6f6;
                    }
                    table.rg-table th {
                    font-weight: bold;
                    padding: 0.35em;
                    font-size: 0.9em;
                    }
                    table.rg-table td {
                    padding: 0.35em;
                    font-size: 0.9em;
                    }
                    table.rg-table .highlight td {
                    font-weight: bold;
                    }
                    table.rg-table th.number,
                    td.number {
                    text-align: right;
                    }

                    /* media queries */
                    @media screen and (max-width: 600px) {
                    .rg-container {
                        max-width: 600px;
                        margin: 0 auto;
                    }
                    table.rg-table {
                        width: 100%;
                    }
                    table.rg-table tr.hide-mobile,
                    table.rg-table th.hide-mobile,
                    table.rg-table td.hide-mobile {
                        display: none;
                    }
                    table.rg-table thead {
                        display: none;
                    }
                    table.rg-table tbody {
                        width: 100%;
                    }
                    table.rg-table tr,
                    table.rg-table th,
                    table.rg-table td {
                        display: block;
                        padding: 0;
                    }
                    table.rg-table tr {
                        border-bottom: none;
                        margin: 0 0 1em 0;
                        padding: 0.5em;
                    }
                    table.rg-table tr.highlight {
                        background-color: inherit !important;
                    }
                    table.rg-table.zebra tr:nth-child(even) {
                        background-color: transparent;
                    }
                    table.rg-table.zebra td:nth-child(even) {
                        background-color: #f6f6f6;
                    }
                    table.rg-table tr:nth-child(even) {
                        background-color: transparent;
                    }
                    table.rg-table td {
                        padding: 0.5em 0 0.25em 0;
                        border-bottom: 1px dotted #ccc;
                        text-align: right;
                    }
                    table.rg-table td[data-title]:before {
                        content: attr(data-title);
                        font-weight: bold;
                        display: inline-block;
                        content: attr(data-title);
                        float: left;
                        margin-right: 0.5em;
                        font-size: 0.95em;
                    }
                    table.rg-table td:last-child {
                        padding-right: 0;
                        border-bottom: 2px solid #ccc;
                    }
                    table.rg-table td:empty {
                        display: none;
                    }
                    table.rg-table .highlight td {
                        background-color: inherit;
                        font-weight: normal;
                    }
                    }

                    </style>
                    <div class='rg-container'>
                        <table class='rg-table zebra' summary='Hed'>
                            <caption class='rg-header'>
                                <span class='rg-hed'>Hed</span>
                                <span class='rg-dek'>Dek goes here.</span>
                            </caption>
                            <thead>
                                <tr>
                                    <th class='text '>Name</th>
                                    <th class='text '>City</th>
                                    <th class='text '>Price</th>
                                    <th class='number '>Rating</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                    <tr class=''>
                                        <td class='text ' data-title='Name'>Superette</td>
                                        <td class='text ' data-title='City'>Holliston</td>
                                        <td class='text ' data-title='Price'>$8.00</td>
                                        <td class='number ' data-title='Rating'>4.5</td>
                                    </tr>
                                

                                    <tr class=''>
                                        <td class='text ' data-title='Name'>Tasty Treat</td>
                                        <td class='text ' data-title='City'>Ashland</td>
                                        <td class='text ' data-title='Price'>$5.00</td>
                                        <td class='number ' data-title='Rating'>2.0</td>
                                    </tr>
                                

                                    <tr class=''>
                                        <td class='text ' data-title='Name'>Big Fresh</td>
                                        <td class='text ' data-title='City'>Framingham</td>
                                        <td class='text ' data-title='Price'>$9.00</td>
                                        <td class='number ' data-title='Rating'>5.0</td>
                                    </tr>
                                

                                    <tr class=''>
                                        <td class='text ' data-title='Name'>Seta's Cafe</td>
                                        <td class='text ' data-title='City'>Watertown</td>
                                        <td class='text ' data-title='Price'>$7.50</td>
                                        <td class='number ' data-title='Rating'>3.8</td>
                                    </tr>
                                
                            </tbody>
                        </table>
                        <div class='rg-source'>
                            <span class='pre-colon'>SOURCE</span>: <span class='post-colon'>Sources</span>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>

                <!-- 附件下載區 -->
                <div class="pagArtinDownloadBk">
                    <div class="mb-60">
                        <h2 class="eleTitCh mb-5">附件下載</h2>
                        <h2 class="eleTitEn">Download Files</h2>
                    </div>
                    <a href="javascript:void(0);" class="eleDownloadList">
                        附件下載附件下載附件下載附件下載附件下載
                    </a>
                </div>
            </div>
        </div>
    </div>

	<?php require('footer.php') ?>
	<!-- line@按鈕 -->
	<?php require('lineBt.php') ?>	
	
</body>
</html>
