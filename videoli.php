<!DOCTYPE html>	
<head>
<title></title>

<!-- 社群連結fb/line -->
<!-- 這裡要套 -->
<meta property="og:url"  content="" />
<meta property="og:type" content="website" />
<meta property="og:site_name" content="" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:locale" content="zh_TW" />
<meta property="og:type" content="website" />
<!-- 抓banner圖 -->
<!-- 如果你分享文章的縮圖要是寬版的大圖的話，那你的圖片至少要大於 600 x 315 px
最大圖片大小不能超過 5MB,圖片的寬高最大不能超過 1500 x 1500 px-->
<meta property="og:image" content="images/ogimages.png" />
<meta property="og:image:type" content="image/png" /> 
<meta property="og:image:width" content="1500" />
<meta property="og:image:height" content="1500" />

<?php require('head.php') ?>

<!-- 這裡要套 -->
<!-- 
    "position": 1,指第一層、"name": "Books",指麵包屑名稱、"item": "https://example.com/books"，指網址
 -->
<!-- JSON-LD 結構化資料 --麵包屑導覽 -->
<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "BreadcrumbList",
    "itemListElement": [{
        "@type": "ListItem",
        "position": 1,
        "name": "Books",
        "item": "https://example.com/books"
    },{
        "@type": "ListItem",
        "position": 2,
        "name": "Science Fiction",
        "item": "https://example.com/books/sciencefiction"
    },{
        "@type": "ListItem",
        "position": 3,
        "name": "Award Winners"
    }]
}
</script>


<script language="javascript">
$(window).ready(function(){
	ScrollTrigger.matchMedia({
		// ipad 
		"(min-width: 768px)": function() {
	
		},
        "(min-width: 1440px)": function() {
	
		},
	  
		// mobile
		"(max-width: 767px)": function() {
	
		},
		  
		// all 
		"all": function() {;
            gsap.set(".js-patPageRedWhiteBg", {
                opacity: 1,
                zIndex: 10,
            });
		}
	}); 

});   
$(window).on('load',function(){
    ScrollTrigger.matchMedia({
		// ipad 
		"(min-width: 768px)": function() {
	
		},
        "(min-width: 1440px)": function() {
	
		},
	  
		// mobile
		"(max-width: 767px)": function() {
	
		},
		  
		// all 
		"all": function() {
            gsap.to(".js-patPageRedWhiteBg", {
				duration: 0.6,
				zIndex: -1,
				background: "#FFF",
				delay: 0,
                opacity: 0,
                ease: "Power3.easeInOut",
			});
		}
	}); 
});  
</script>

<body class="">

    <div class="patPageRedWhiteBg js-patPageRedWhiteBg"></div>
    <!-- loading.php拿掉，改logo.php -->
	<?php require('logo.php') ?>

	<!-- 手機視口導覽列 -->
	<?php require('smlHeader.php') ?>
	<!-- 電腦視口導覽列 -->
	<?php require('header.php') ?>

	
	<!-- 頁面內容 -->
	<div class="patPageContentWidth">
        <!-- banner區 -->
        <div class="max-width--1640">
            <div class="patPageBannerBk">
                <!-- 漫畫 -->
                <img src="images/pageBg.svg" alt="背景" class="patPageBanner-bg">
                <div class="patPageComic">
                    <img src="images/photo07.png" alt="人" class="patPageComic-img">
                    <p class="patPageComic-diaStyle patPageComic-diaStyle--style01">黑傑克名言：<br />"規劃保單是簡單的事"</p>
                    <p class="patPageComic-diaStyle patPageComic-diaStyle--style02">＂...只要你跟我一樣，<br />看過上千張保單條款＂</p>
                </div>
                
                <div class="patPageTitBiteBk pagVideoli-biteBk">
                    <!-- 麵包屑 -->
                    <article class="eleBite mb-25">
                        <a href="index.php" class="eleBite-link">
                            首頁
                        </a>
                        <a href="videoli.php" class="eleBite-link">
                            影音專區
                        </a>
                        <a href="videoli.php" class="eleBite-link">
                            理賠案例
                        </a>
                    </article>
                    <div class="">
                        <h2 class="eleTitCh mb-5">理賠案例</h2>
                        <h2 class="eleTitEn">Video classify</h2>
                    </div>
                </div>
            </div>
	    </div>

        <!-- 文章列表 -->
        <div class="max-width--1640">
            <div class="pagVideoliListBk">

                <!-- 1頁13個 -->
                <!-- 影片 -->
                <!-- 第一個class:pagVideoList--lates -->
                <article class="pagVideoList--latest">
                    <img src="images/latest-video-label.svg" alt="label" class="pagVideoliList--latestLabel js-pagVideoliList--latestLabel">
                    <a href="videoin.php" class="pagVideoList-imgBk--latest">
                        <div class="baseVideoBk">
                            <img src="images/videImg001.png" alt="img" class="eleImgBk-img">
                        </div>
                        <!-- <img src="images/video-bt01.svg" alt="video icon" class="pagVideoList-imgBk--videoBt"> -->
                    </a>
                    <div class="pagVideoList-textBk--latest">
                        <div class="">
                            <a href="videoli.php" class="eleLabel pagVideoList-textBk--label">理賠案例</a>
                        </div>
                        <a href="videoin.php" class="pagVideoList-textBk--tit">保險公司的成本跟考核，是業務員的責任？</a>
                        <p class="eleDate pagVideoList-textBk--date">2021.05.30</p>
                        <p class="pagVideoList-textBk--text">10多年來，我常在網路上替網友分析保單、保險理賠的各種情境。您可能在論壇跟部落格都曾經看過我的文章或影片。</p>
                    </div>
                </article>
                <!-- 一般影片列表 -->
                <!-- 放4個 -->
                <article class="pagVideoList">
                    <a href="videoin.php" class="pagVideoList-imgBk">
                        <div class="baseVideoBk">
                        <img src="images/videImg002.png" alt="img" class="eleImgBk-img">
                        </div>
                    </a>
                    <div class="pagVideoList-textBk">
                        <div class="">
                            <a href="articleli.php" class="eleLabel pagVideoList-textBk--label">理賠案例</a>
                        </div>
                        <a href="articlein.php" class="pagVideoList-textBk--tit">保險公司的成本跟考核，是業務員的責任？</a>
                        <p class="eleDate pagVideoList-textBk--date">2021.05.30</p>
                        <p class="pagVideoList-textBk--text">10多年來，我常在網路上替網友分析保單、保險理賠的各種情境。您可能在論壇跟部落格都曾經看過我的文章或影片。</p>

                    </div>
                </article>
                <article class="pagVideoList">
                    <a href="videoin.php" class="pagVideoList-imgBk">
                        <div class="baseVideoBk">
                            <img src="images/videImg.webp" alt="img" class="eleImgBk-img">
                        </div>
                    </a>
                    <div class="pagVideoList-textBk">
                        <div class="">
                            <a href="articleli.php" class="eleLabel pagVideoList-textBk--label">理賠案例</a>
                        </div>
                        <a href="articlein.php" class="pagVideoList-textBk--tit">保險公司的成本跟考核</a>
                        <p class="eleDate pagVideoList-textBk--date">2021.05.30</p>
                        <p class="pagVideoList-textBk--text">10多年來，我常在網路上替網友分析保單、保險理賠的各種</p>
                    </div>
                </article>
                <article class="pagVideoList">
                    <a href="videoin.php" class="pagVideoList-imgBk">
                        <div class="baseVideoBk">
                            <img src="images/videImg.webp" alt="img" class="eleImgBk-img">
                        </div>
                    </a>
                    <div class="pagVideoList-textBk">
                        <div class="">
                            <a href="articleli.php" class="eleLabel pagVideoList-textBk--label">理賠案例</a>
                        </div>
                        <a href="articlein.php" class="pagVideoList-textBk--tit">保險公司的成本跟考核，是業務員的責任？</a>
                        <p class="eleDate pagVideoList-textBk--date">2021.05.30</p>
                        <p class="pagVideoList-textBk--text">10多年來，我常在網路上替網友分析保單、保險理賠的各種情境。您可能在論壇跟部落格都曾經看過我的文章或影片。</p>
                    </div>
                </article>
                <article class="pagVideoList">
                    <a href="videoin.php" class="pagVideoList-imgBk">
                        <div class="baseVideoBk">
                            <img src="images/videImg.webp" alt="img" class="eleImgBk-img">
                        </div>
                    </a>
                    <div class="pagVideoList-textBk">
                        <div class="">
                            <a href="articleli.php" class="eleLabel pagVideoList-textBk--label">理賠案例</a>
                        </div>
                        <a href="articlein.php" class="pagVideoList-textBk--tit">保險公司的成本跟考核，是業務員的責任？</a>
                        <p class="eleDate pagVideoList-textBk--date">2021.05.30</p>
                        <p class="pagVideoList-textBk--text">10多年來，我常在網路上替網友分析保單、保險理賠的各種情境。您可能在論壇跟部落格都曾經看過我的文章或影片。</p>
                    </div>
                </article>
            </div>
        </div>
        <!-- 分頁區 -->
        <div class="eleSelPageBk">
            <a class="eleSelPageArrow eleSelPageArrow--left"></a>
            <a href="javascript:void(0);" title="第一頁" class="eleSelPageCount eleSelPageCount--pageIn mlr-3">1</a>
            <a href="javascript:void(0);" title="第一頁" class="eleSelPageCount mlr-3">2</a>
            <a href="javascript:void(0);" title="第一頁" class="eleSelPageCount mlr-3">3</a>
            <a href="javascript:void(0);" title="第一頁" class="eleSelPageCount mlr-3">4</a>
            <a href="javascript:void(0);" title="第一頁" class="eleSelPageCount mlr-3">5</a>
            <a class="eleSelPageArrow eleSelPageArrow--right"></a>
        </div>

        <?php require('footer.php') ?>
        <!-- line@按鈕 -->
        <?php require('lineBt.php') ?>	
    </div>


	
</body>
</html>
