<script>
$(window).on('load',function(){
    gsap.set(".js-eleLineBt-man", {
        opacity: 0,
        y: 40,
    });
    gsap.set(".js-eleLineBt-dia", {
        opacity: 0,
        transformOrigin: "0% 0%",
	    transform: "scale(0.0)",
    });

    var tllineBt = gsap.timeline();
    tllineBt.to(".js-eleLineBt-man", {
		opacity: 1,
        y: 0,
		delay: 10,
		duration: 0.7,
        ease: "power4.out",
	});     
    tllineBt.to(".js-eleLineBt-dia", {
		scaleX: 1,
		scaleY: 1,
		transformOrigin: "50% 100%",
		opacity: 1,
		duration: 0.5,
        ease: "Bounce.easeOut",
	});     
});
</script>


<a href="javascript:void(0);" class="eleLineBt">
    <img src="images/line-man.png" alt="man" class="eleLineBt-man js-eleLineBt-man">
    <img src="images/line-dia.svg" alt="dia" class="eleLineBt-dia js-eleLineBt-dia">
</a>