<!DOCTYPE html>	
<head>
<title></title>

<!-- 社群連結fb/line -->
<!-- 這裡要套 -->
<meta property="og:url"  content="" />
<meta property="og:type" content="website" />
<meta property="og:site_name" content="" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:locale" content="zh_TW" />
<meta property="og:type" content="website" />
<!-- 抓banner圖 -->
<!-- 如果你分享文章的縮圖要是寬版的大圖的話，那你的圖片至少要大於 600 x 315 px
最大圖片大小不能超過 5MB,圖片的寬高最大不能超過 1500 x 1500 px-->
<meta property="og:image" content="images/ogimages.png" />
<meta property="og:image:type" content="image/png" /> 
<meta property="og:image:width" content="1500" />
<meta property="og:image:height" content="1500" />

<?php require('head.php') ?>


<!-- 首頁輪播 -->
<!-- <link href="vendor/Owl/owl.carousel.css" rel="stylesheet" type="text/css" media="all" /> -->
<!-- <link rel="stylesheet" href="vendor/Owl/owl.theme.default.css"> -->
<!-- <script src="vendor/Owl/owl.carousel.js"></script> -->
<!-- <script language="javascript">
    $(document).ready(function() {
        $('.owl-carousel').owlCarousel({
            animateOut: 'fadeOut',
            animateIn: 'flipInX',
            loop: true,
            margin: 0,
            stagePadding: 0,
            smartSpeed: 300,
            dots: false,
            nav:  true,
            responsive: {
                320: {
                    items: 1
                },
                1024: {
                    items: 2
                },
                1440: {
                    items: 2,
                    margin: 50,
                },
                1860:{
                    items: 3,
                    margin: 50,
                }
            }
        });
    });
</script> -->

<script language="javascript">
$(window).on('load',function(){
	ScrollTrigger.matchMedia({
		// ipad 
		"(min-width: 768px)": function() {
	
		},
        "(min-width: 1440px)": function() {
	
		},
	  
		// mobile
		"(max-width: 767px)": function() {
	
		},
		  
		// all 
		"all": function() {
			gsap.to(".js-patLoadingBg", {
				duration: 1,
				zIndex: 1,
				ease: "Power3.easeInOut",
			});
			//comicBanner
			// gsap.set(".js-indComic-frame01", {
			// 	opacity: 0,
			// 	y: 20,
			// });
			gsap.set(".js-indComic-narrBk", {
				opacity: 0,
				y: 30,
			});
			// gsap.set(".js-indComic-frame02", {
			// 	opacity: 0,
			// 	y: 20,
			// });
			// gsap.set(".js-indComic-frame03", {
			// 	opacity: 0,
			// 	y: 20,
			// });
			gsap.set(".js-indComic-frame01--diaImg", {
				transformOrigin: "0% 0%",
	    		transform: "scale(0.0)",
				opacity: 0,
			});
			gsap.set(".js-indComic-frame02--diaImg", {
				transformOrigin: "0% 0%",
	    		transform: "scale(0.0)",
				opacity: 0,
			});
			gsap.set(".js-indComic-frame03--diaImg", {
				transformOrigin: "0% 0%",
	    		transform: "scale(0.0)",
				opacity: 0,
			});
			
			var tlcomic = gsap.timeline();
			tlcomic.to(".js-indComic-frame01", {
				opacity: 1,
				y: 0,
				duration: 0.4,
				delay: 2.5,
				ease: "Power3.easeInOut",
			});
			tlcomic.to(".js-indComic-narrBk", {
				opacity: 1,
				y: 0,
				duration: 0.4,
				delay: 0,
				ease: "Power3.easeInOut",
			});
			tlcomic.to(".js-indComic-frame01--diaImg", {
				scaleX: 1,
				scaleY: 1,
				transformOrigin: "100% 60%",
				opacity: 1,
				duration: 0.3,
				ease: "expo.out",
			});
			tlcomic.to(".js-indComic-frame02--diaImg", {
				scaleX: 1,
				scaleY: 1,
				transformOrigin: "50% 50%",
				opacity: 1,
				duration: 0.4,
				ease: "Power2.easeInOut",
			});
			tlcomic.to(".js-indComic-frame03--diaImg", {
				scaleX: 1,
				scaleY: 1,
				transformOrigin: "50% 50%",
				opacity: 1,
				duration: 0.3,
				ease: "Power2.easeInOut",
			});
		    
			// 第二區漫畫banner
			gsap.set(".js-indComic02-ele01", {
				transformOrigin: "0% 0%",
	    		transform: "scale(0.0)",
				opacity: 0,
			});
			gsap.set(".js-indComic02-ele02", {
				transformOrigin: "0% 0%",
	    		transform: "scale(0.0)",
				opacity: 0,
			});
			gsap.set(".js-indComic02-ele03", {
				transformOrigin: "0% 0%",
	    		transform: "scale(0.0)",
				opacity: 0,
			});

			gsap.to(".js-indComic02-ele03", {
				scrollTrigger: {
					trigger: ".js-indComic02-ele03",
					start: "center center-=100", 
					endTrigger: ".js-indComic02-ele03", 
					end: "bottom+=200 top",
					markers: false,
					scrub: false,
					toggleActions: "play pause resume pause",
					once: true,
				},
				scaleX: 1,
				scaleY: 1,
				transformOrigin: "50% 100%",
				opacity: 1,
				duration: 0.6,
				ease: "Power2.easeInOut",
			});
			gsap.to(".js-indComic02-ele01", {
				scrollTrigger: {
					trigger: ".js-indComic02-ele01",
					start: "top-=250 center-=300", 
					endTrigger: ".js-indComic02-ele02", 
					end: "bottom+=200 top",
					markers: false,
					scrub: false,
					toggleActions: "play pause resume pause",
					once: true,
				},
				scaleX: 1,
				scaleY: 1,
				transformOrigin: "100% 100%",
				opacity: 1,
				duration: 0.8,
				ease: "Bounce.easeOut",
			});
			gsap.to(".js-indComic02-ele02", {
				scrollTrigger: {
					trigger: ".js-indComic02-ele01",
					start: "top-=250 center-=300", 
					endTrigger: ".js-indComic02-ele02", 
					end: "bottom+=200 top",
					markers: false,
					scrub: false,
					toggleActions: "play pause resume pause",
					once: true,
				},
				scaleX: 1,
				scaleY: 1,
				transformOrigin: "0% 100%",
				opacity: 1,
				duration: 1,
				ease: "Bounce.easeOut",
			});
			

			//新手上路區
			gsap.set(".js-indqaBk-tit", {
				transformOrigin: "0% 0%",
	    		transform: "scale(0.0)",
				opacity: 0,
			});
			gsap.to(".js-indqaBk-tit", {
				scrollTrigger: {
					trigger: ".js-indqaBk-tit",
					start: "top center+=200", 
					endTrigger: ".js-indqaBk-tit", 
					end: "bottom+=250 top",
					markers: false,
					scrub: false,
					toggleActions: "play pause resume pause",
					once: true,
				},
				scaleX: 1,
				scaleY: 1,
				transformOrigin: "50% 50%",
				opacity: 1,
				duration: 0.7,
				ease: "Bounce.easeOut",
			});
			gsap.set(".js-indqaBk-listStyle--starEle", {
				transformOrigin: "0% 0%",
	    		transform: "scale(0.0)",
				opacity: 0,
			});
			gsap.to(".js-indqaBk-listStyle--starEle", {
				scrollTrigger: {
					trigger: ".js-indqaBk-listStyle--starEle02",
					start: "top center+=150", 
					endTrigger: ".js-indqaBk-listStyle--starEle02", 
					end: "bottom+=150 top",
					markers: false,
					scrub: false,
					toggleActions: "play pause resume pause",
					once: true,
				},
				scaleX: 1,
				scaleY: 1,
				transformOrigin: "50% 50%",
				opacity: 1,
				duration: 0.3,
				ease: "rough",
			});
			gsap.set(".js-indqaBk-listStyle--diaEle01", {
				transformOrigin: "0% 0%",
	    		transform: "scale(0.0)",
				opacity: 0,
			});
			gsap.to(".js-indqaBk-listStyle--diaEle01", {
				scrollTrigger: {
					trigger: ".js-indqaBk-listStyle--diaEle01",
					start: "top-=50 center+=150", 
					endTrigger: ".js-indqaBk-listStyle--diaEle01", 
					end: "bottom+=150 top",
					markers: false,
					scrub: false,
					toggleActions: "play pause resume pause",
					once: true,
				},
				scaleX: 1,
				scaleY: 1,
				transformOrigin: "50% 50%",
				opacity: 1,
				duration: 1.5,
				ease: "power4.inOut",
			});
			gsap.to(".js-indqaBk-listStyle--diaEle01", {
				yoyo: true,
				repeat: -1,
				y: 15,
				duration: 5,
				ease: "power1.inOut",
			});
		}
	}); 

});   
</script>

<body class="">
	<!-- loading動畫 -->
	<?php require('loading.php') ?>
	<!-- 手機視口導覽列 -->
	<?php require('smlHeader.php') ?>
	<!-- 電腦視口導覽列 -->
	<?php require('header.php') ?>
	

	<!-- 首頁漫畫comic banner -->
	<div class="max-width--1640">
		<div class="indComicBk">
			<section class="indComic-frameStyle indComic-frame01 js-indComic-frame01">
				<p class="indComic-frameStyle--number">1</p>
				<img src="images/photo01.png" alt="photo01" class="indComic-frame01--manImg">
				<img src="images/ele01.svg" alt="買對保險，百倍奉還！" class="indComic-frame01--diaImg js-indComic-frame01--diaImg">
			</section>
			<section class="indComic-narrBk js-indComic-narrBk">
				<p class="indComic-narrStyle">
					大家好， <br />
					我叫蔡銘賢，<br />
					人稱保險黑傑克。
				</p>
				<span class="indComic-narrStyle indComic-narrStyle--shadow"></span>
			</section>
			<section class="indComic-frameStyle indComic-frame02 js-indComic-frame01">
				<p class="indComic-frameStyle--number">2</p>
				<img src="images/photo02.png" alt="photo02" class="indComic-frame02--manImg">
				<p class="indComic-frame02--diaImg js-indComic-frame02--diaImg">
					網友問這張保單的<br />
					理賠項目合理嗎 ?<br />
					我研究看看...<br />
				</p>
			</section>
			<section class="indComic-frameStyle indComic-frame03  js-indComic-frame01">
				<p class="indComic-frameStyle--number">3</p>
				<img src="images/photo03.png" alt="photo03" class="indComic-frame03--manImg">
				<p class="indComic-frame03--diaImg js-indComic-frame03--diaImg">
					精算之後我推薦<br />
					這幾張保單組合<br />
					最划算 !<br />
				</p>
			</section>
			<div class="clear"></div>
		</div>
	</div>

	<!-- 第二區漫畫banner跟新手QA區 -->
	<div class="indComicBk02">
		<img src="images/photo06.png" alt="photo06" class="indComic02-manImg">
		<img src="images/ele03-word.svg" alt="保險保單條款分析" class="indComic02-ele01 js-indComic02-ele01">
		<img src="images/ele04-word.svg" alt="保險理賠經驗分享" class="indComic02-ele02 js-indComic02-ele02">
		<img src="images/ele05.svg" alt="有保險問題盡量問！" class="indComic02-ele03 js-indComic02-ele03">
		<div class="indComic-frameStyle indComic02-frame04">
			<p class="indComic02-frameStyle--number01">4</p>
			<p class="indComic02-frame04--text">
				10多年來，我常在網路上替網友分析保單、保險理賠的各種情境。您可能在論壇跟部落格都曾經看過我的文章或影片。
			</p>
		</div>
		<div class="indComic-frameStyle indComic02-frame05">
			<p class="indComic02-frameStyle--number02">5</p>
			<p class="indComic02-frame05--text">
				我發現大部分的網友對保險都有一些需要釐清的觀念。我想把多年來累積的知識經驗，透過整理，讓大家對保險有清楚而正確的認識。
			</p>
		</div>
		<img src="images/pageBg.svg" alt="背景" class="indComic02-bg01">
		<img src="images/pageBg.svg" alt="背景" class="indComic02-bg02">
		<img src="images/pageBg-color.svg" alt="背景色" class="indComic02-bg03">

		<!-- 新手QA區 -->
		<div class="indqaBk">
			<img src="images/ele08.svg" alt="保險新手看這邊！" class="indqaBk-tit js-indqaBk-tit">
			<!-- 列表 -->
			<div class="indqaBk-listBk">
				<a href="qain.php" class="indqaBk-listStyle" title="車禍理賠相關問題">
					<p class="indqaBk-listStyle--number">1</p>
					車禍理賠相關問題
				</a>
				<a href="qain.php" class="indqaBk-listStyle" title="車禍理賠相關問題">
					<p class="indqaBk-listStyle--number">2</p>
					車禍理賠相關問題
				</a>
				<a href="qain.php" class="indqaBk-listStyle" title="車禍理賠相關問題">
					<p class="indqaBk-listStyle--number">3</p>
					車禍理賠相關問題
				</a>
				<a href="qain.php" class="indqaBk-listStyle" title="車禍理賠相關問題">
					<p class="indqaBk-listStyle--number">4</p>
					車禍理賠相關問題
				</a>
				<a href="qain.php" class="indqaBk-listStyle" title="車禍理賠相關問題">
					<p class="indqaBk-listStyle--number">5</p>
					車禍理賠相關問題
				</a>
			</div>
			<img src="images/ele07.svg" alt="star01" class="indqaBk-listStyle--starEle01 js-indqaBk-listStyle--starEle">
			<img src="images/ele07.svg" alt="star02" class="indqaBk-listStyle--starEle02 js-indqaBk-listStyle--starEle js-indqaBk-listStyle--starEle02">
			<img src="images/ele07.svg" alt="star03" class="indqaBk-listStyle--starEle03 js-indqaBk-listStyle--starEle">
			<img src="images/ele09.svg" alt="框01" class="indqaBk-listStyle--diaEle01 js-indqaBk-listStyle--diaEle01">
			<img src="images/ele011.svg" alt="框01元素" class="indqaBk-listStyle--diaEle02 ">
			<img src="images/ele010.svg" alt="框02" class="indqaBk-listStyle--diaEle03 js-indqaBk-listStyle--diaEle01">
			<img src="images/ele012.svg" alt="框02元素" class="indqaBk-listStyle--diaEle04 ">
			<div class="clear"></div>
		</div>
	</div>

	<!-- 理賠案例 -->
	<div class="max-width--1640">
		<div class="indCaseBk">
			<div class="mb-60 plr-20">
				<h2 class="eleTitCh mb-5">理賠案例</h2>
				<h2 class="eleTitEn">Claim cases</h2>
			</div>
			<!-- 文章 -->
			<article class="eleArticleList">
				<!-- <a href="articleli.php" class="eleArticleList-label">理賠案例</a> -->
				<a href="articlein.php" class="eleArticleList-imgBk baseImgBk">
					<img src="images/article01.png" alt="img" class="eleImgBk-img">
				</a>
				<div class="eleArticleList-textBk">
					<a href="articlein.php" class="eleArticleList-textBk--tit">保險知識文章標題保險知識文章標題保險知識文章標題保險知識文章標題保險知識文章標題保險知識文章標題</a>
					<p class="eleArticleList-textBk--date">2021.05.30</p>
				</div>
			</article>
			<article class="eleArticleList">
				<a href="articlein.php" class="eleArticleList-imgBk baseImgBk">
					<img src="images/article02.png" alt="img" class="eleImgBk-img">
				</a>
				<div class="eleArticleList-textBk">
					<a href="articlein.php" class="eleArticleList-textBk--tit">保險知識文章標題</a>
					<p class="eleArticleList-textBk--date">2021.05.30</p>
				</div>
			</article>
			<article class="eleArticleList">
				<a href="articlein.php" class="eleArticleList-imgBk baseImgBk">
					<img src="images/article01.png" alt="img" class="eleImgBk-img">
				</a>
				<div class="eleArticleList-textBk">
					<a href="articlein.php" class="eleArticleList-textBk--tit">保險知識文章標題保險知識文章標題保險知識文章標題保險知識文章標題保險知識文章標題保險知識文章標題</a>
					<p class="eleArticleList-textBk--date">2021.05.30</p>
				</div>
			</article>
			<article class="eleArticleList">
				<a href="articlein.php" class="eleArticleList-imgBk baseImgBk">
					<img src="images/article02.png" alt="img" class="eleImgBk-img">
				</a>
				<div class="eleArticleList-textBk">
					<a href="articlein.php" class="eleArticleList-textBk--tit">保險知識文章標題保險知識</a>
					<p class="eleArticleList-textBk--date">2021.05.30</p>
				</div>
			</article>
		</div>
		<div class="clear"></div>
	</div>

	<?php require('footer.php') ?>
	<!-- line@按鈕 -->
	<?php require('lineBt.php') ?>	
	
</body>
</html>
