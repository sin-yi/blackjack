<!DOCTYPE html>	
<head>
<title></title>

<!-- 社群連結fb/line -->
<!-- 這裡要套 -->
<meta property="og:url"  content="" />
<meta property="og:type" content="website" />
<meta property="og:site_name" content="" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:locale" content="zh_TW" />
<meta property="og:type" content="website" />
<!-- 抓banner圖 -->
<!-- 如果你分享文章的縮圖要是寬版的大圖的話，那你的圖片至少要大於 600 x 315 px
最大圖片大小不能超過 5MB,圖片的寬高最大不能超過 1500 x 1500 px-->
<meta property="og:image" content="images/ogimages.png" />
<meta property="og:image:type" content="image/png" /> 
<meta property="og:image:width" content="1500" />
<meta property="og:image:height" content="1500" />

<?php require('head.php') ?>

<!-- 這裡要套 -->
<!-- 
    "position": 1,指第一層、"name": "Books",指麵包屑名稱、"item": "https://example.com/books"，指網址
 -->
<!-- JSON-LD 結構化資料 --麵包屑導覽 -->
<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "BreadcrumbList",
    "itemListElement": [{
        "@type": "ListItem",
        "position": 1,
        "name": "Books",
        "item": "https://example.com/books"
    },{
        "@type": "ListItem",
        "position": 2,
        "name": "Science Fiction",
        "item": "https://example.com/books/sciencefiction"
    },{
        "@type": "ListItem",
        "position": 3,
        "name": "Award Winners"
    }]
}
</script>

<script language="javascript">
$(window).ready(function(){
	ScrollTrigger.matchMedia({
		// ipad 
		"(min-width: 768px)": function() {
	
		},
        "(min-width: 1440px)": function() {
	
		},
	  
		// mobile
		"(max-width: 767px)": function() {
	
		},
		  
		// all 
		"all": function() {;
            gsap.set(".js-patPageRedWhiteBg", {
                opacity: 1,
                zIndex: 10,
            });
		}
	}); 

});   
$(window).on('load',function(){
    ScrollTrigger.matchMedia({
		// ipad 
		"(min-width: 768px)": function() {
	
		},
        "(min-width: 1440px)": function() {
	
		},
	  
		// mobile
		"(max-width: 767px)": function() {
	
		},
		  
		// all 
		"all": function() {
            gsap.to(".js-patPageRedWhiteBg", {
				duration: 0.6,
				zIndex: -1,
				background: "#FFF",
				delay: 0,
                opacity: 0,
                ease: "Power3.easeInOut",
			});
		}
	}); 
});  
</script>

<body class="">
    <div class="patPageRedWhiteBg js-patPageRedWhiteBg"></div>
    <!-- loading.php拿掉，改logo.php -->
	<?php require('logo.php') ?>

	<!-- 手機視口導覽列 -->
	<?php require('smlHeader.php') ?>
	<!-- 電腦視口導覽列 -->
	<?php require('header.php') ?>
    
    <div class="patRedTop-bg js-patRedTop-bg"></div>
	
	<!-- 頁面內容 -->
	<div class="patPageContentWidth js-contentShow">
        <div class="max-width--1640">
            <!-- banner區 -->
            <div class="patPageTitBiteBk--twoColumn">
                <div class="patPageTitBiteBk">
                    <!-- 麵包屑 -->
                    <article class="eleBite mb-25">
                        <a href="index.php" class="eleBite-link">
                            首頁
                        </a>
                        <a href="about.php" class="eleBite-link">
                            關於黑傑克
                        </a>
                        <a href="about.php" class="eleBite-link">
                            寫在規劃保單之前
                        </a>
                    </article>
                    <div class="">
                        <h2 class="eleTitCh mb-5">寫在規劃保單之前</h2>
                        <h2 class="eleTitEn">About Black jack</h2>
                    </div>
                </div>
            </div>
            
            <div class="patTwoColumnBk">
                <!-- 文章左側區 -->
                <div class="patTwoColumnBk-sidebarBk">
                    <!-- <h4 class="patSidebarBk-tit mb-10">關於黑傑克</h4> -->
                    <!-- 一篇文章 -->
                    <a href="about.php" class="patSidebarBk-link patSidebarBk-link--active">
                        <span class="patSidebarBk-link--number">1</span>
                        <span class="patSidebarBk-link--tit">寫在規劃保單之前</span>
                    </a>
                    <!-- 一篇文章 -->
                    <a href="about.php" class="patSidebarBk-link">
                        <span class="patSidebarBk-link--number">2</span>
                        <span class="patSidebarBk-link--tit">寫在規劃保單之前</span>
                    </a>
                    <!-- 一篇文章 -->
                    <a href="about.php" class="patSidebarBk-link">
                        <span class="patSidebarBk-link--number">3</span>
                        <span class="patSidebarBk-link--tit">寫在規劃保單之前</span>
                    </a>
                </div>

                <!-- 文章區 -->
                <div class="patTwoColumnBk-contentBk">
                    <!-- 修改 去掉textBk -->
                    <div class="unreset">
                        <!-- 文編放置區 -->
                        <img src="images/banner01.png" alt="" class="" width="100%">
                        <br />
                        <br />
                        <br />
                        <p>
                            保險公司一張保單的成本，內容有什麼？傳統保險公司跟保險經紀公司（簡稱保經）兩種制度，對於保單成本的影響，會讓業務人員的分紅以及職涯產生何種差異？這篇文章能讓想要了解從事保險業務的人，分析自己的需求，選擇適合自己條件的制度。
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>


	<?php require('footer.php') ?>
	<!-- line@按鈕 -->
	<?php require('lineBt.php') ?>	
	
</body>
</html>
