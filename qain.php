<!DOCTYPE html>	
<head>
<title></title>

<!-- 社群連結fb/line -->
<meta property="og:url"  content="" />
<meta property="og:type" content="website" />
<meta property="og:site_name" content="" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<!-- 抓banner圖 -->
<meta property="og:image" content="" />
<meta property="og:image:type" content="image/png" /> 
<!-- 如果你分享文章的縮圖要是寬版的大圖的話，那你的圖片至少要大於 600 x 315 px
最大圖片大小不能超過 5MB,圖片的寬高最大不能超過 1500 x 1500 px-->
<meta property="og:image:width" content="" />
<meta property="og:image:height" content="" />

<?php require('head.php') ?>

<!-- JSON-LD 結構化資料 --文章 -->
<script type="application/ld+json">
    {
        "@context": "https://schema.org",
        "@type": "NewsArticle",
        "headline": "Article headline",
        "image": [
            "https://example.com/photos/1x1/photo.jpg",
            "https://example.com/photos/4x3/photo.jpg",
            "https://example.com/photos/16x9/photo.jpg"
        ],
        "datePublished": "2015-02-05T08:00:00+08:00",
        "dateModified": "2015-02-05T09:20:00+08:00"
    }
</script>


<script language="javascript">
$(window).ready(function(){
	ScrollTrigger.matchMedia({
		// ipad 
		"(min-width: 768px)": function() {
	
		},
        "(min-width: 1440px)": function() {
	
		},
	  
		// mobile
		"(max-width: 767px)": function() {
	
		},
		  
		// all 
		"all": function() {;
            gsap.set(".js-patPageRedWhiteBg", {
                opacity: 1,
                zIndex: 10,
            });
		}
	}); 

});   
$(window).on('load',function(){
    ScrollTrigger.matchMedia({
		// ipad 
		"(min-width: 768px)": function() {
	
		},
        "(min-width: 1440px)": function() {
	
		},
	  
		// mobile
		"(max-width: 767px)": function() {
	
		},
		  
		// all 
		"all": function() {
            gsap.to(".js-patPageRedWhiteBg", {
				duration: 0.6,
				zIndex: -1,
				background: "#FFF",
				delay: 0,
                opacity: 0,
                ease: "Power3.easeInOut",
			});
		}
	}); 
});  
</script>

<body class="">
    <div class="patPageRedWhiteBg js-patPageRedWhiteBg"></div>
    <!-- loading.php拿掉，改logo.php -->
	<?php require('logo.php') ?>
    
	<!-- 手機視口導覽列 -->
	<?php require('smlHeader.php') ?>
	<!-- 電腦視口導覽列 -->
	<?php require('header.php') ?>

    <div class="pagQain-bg js-pagQain-bg"></div>
	
	<!-- 頁面內容 -->
	<div class="patPageContentWidth js-contentShow">
        <!-- banner區 -->
        <div class="max-width--1640">
            <div class="pagQainBannerBk">
                <img src="images/ele015.svg" alt="" class="pagQainBannerBk-ele01">
                <img src="images/ele013.svg" alt="" class="pagQainBannerBk-ele02">
                
                <div class="patPageTitBiteBk pagQainBannerBk-biteBk">
                    <!-- 麵包屑 -->
                    <article class="eleBite mb-25">
                        <a href="index.php" class="eleBite-link">
                            首頁
                        </a>
                        <a href="qali.php" class="eleBite-link">
                            常見問題區
                        </a>
                        <a href="qali.php" class="eleBite-link">
                            保險新手看這邊
                        </a>
                    </article>
                    <div class="">
                        <h2 class="eleTitCh mb-5">保險新手看這邊</h2>
                        <h2 class="eleTitEn">Q & A classify</h2>
                    </div>
                </div>

                <!-- 文章標題資訊 -->
                <div class="pagQainBanner-artInfo">
                    <h1 class="pagQainBanner-artInfo--tit">常見申請理賠流程常見申請理賠流程常見申請理賠流程常見申請理賠流程</h1>
                </div>
            </div>
	    </div>

        <!-- 文章區 -->
        <div class="max-width--1640 overflow-hidden">
            <div class="pagQainTextBk">
                <div class="unreset textBk">
                    <!-- 文編放置區 -->
                    <p>
                        保險公司一張保單的成本，內容有什麼？傳統保險公司跟保險經紀公司（簡稱保經）兩種制度，對於保單成本的影響，會讓業務人員的分紅以及職涯產生何種差異？這篇文章能讓想要了解從事保險業務的人，分析自己的需求，選擇適合自己條件的制度。
                    </p>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <?php require('footer.php') ?>
        <!-- line@按鈕 -->
	    <?php require('lineBt.php') ?>	
    </div>



	
</body>
</html>
