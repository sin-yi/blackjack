<footer class="patFooter">
	<a href="index.php" class="patFooter-logo">
		<img src="images/logo.svg" alt="logo" class="" width="100%">
	</a>
	<div class="patFooter-infoBk">
		<p class="patFooter-infoBk--copyRight mb-5">© 2021 all rights reserved</p>
		<a href="https://www.cosmosdesign.tw" class="patFooter-infoBk--design">
			Design by 
			<mark>COSMOS</mark>
		</a>
	</div>
	<!-- 社群連結 -->
	<div class="patFooter-mediaBk">
		<a href="javascript:void(0);" class="patFooter-mediaBk--btStyle">
			<img src="images/line-icon.svg" alt="line" class="">
		</a>
		<a href="javascript:void(0);" class="patFooter-mediaBk--btStyle">
			<img src="images/twitter-icon.svg" alt="twitter" class="">
		</a>
		<a href="javascript:void(0);" class="patFooter-mediaBk--btStyle">
			<img src="images/fb-icon.svg" alt="fb" class="">
		</a>
		<a href="javascript:void(0);" class="patFooter-mediaBk--btStyle">
			<img src="images/podcast-icon.svg" alt="podcast" class="">
		</a>
	
	</div>
</footer>