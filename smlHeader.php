<script language="javascript">
$(document).ready(function() {	

	/*小視口導覽列開合*/
	$(".jsNavSml-bt").on('click',function(){
		if($(".jsNavSmall").css("opacity") == "0"){
			// $(".jsNavSmall").slideDown("slow");
			// $(".jsNavSmall").css("display" , "block");
			$(".jsNavSmall").animate({
				right: "0px",
				opacity: 1,
			},500);
			$(".js-patSmlNav-layer").css("z-index", "4");
			$(".js-patSmlNav-layer").animate({
				opacity: 0.8,
				// z-index: 4,
			},500);
			gsap.set(".jsAccording", {
				opacity: 0,
				x: -40,
			});
			gsap.to(".jsAccording", {
				opacity: 1,
				x: 0,
				stagger: 0.2,
				delay: 0.5,
			});
			$(".patSmlHeader-navbt--icon01").addClass("patSmlHeader-navbt--icon01--rotate");
			$(".patSmlHeader-navbt--icon02").addClass("patSmlHeader-navbt--icon02--rotate");
		}else{
			// $(".jsNavSmall").slideUp("slow");
			$(".jsNavSmall").animate({
				right: "-300px",
				opacity: 0,
			},500);
			$(".js-patSmlNav-layer").css("z-index", "-1");
			$(".js-patSmlNav-layer").animate({
				opacity: 0,
			},500);
			// $(".jsNavSmall").css("display" , "none");
			$(".patSmlHeader-navbt--icon01").removeClass("patSmlHeader-navbt--icon01--rotate");
			$(".patSmlHeader-navbt--icon02").removeClass("patSmlHeader-navbt--icon02--rotate");
		}
	});

});
$(window).on('load',function(){
	ScrollTrigger.matchMedia({
		// ipad 
		"(min-width: 768px)": function() {
	
		},
        "(min-width: 1440px)": function() {
	
		},
	  
		// mobile
		"(max-width: 767px)": function() {

		},
		  
		// all 
		"all": function() {
			gsap.to(".jsNavSml", {
				delay: 1.5,
				duration: 0.5,
				opacity: "1",
				ease: "Power3.easeInOut",
			});			
		}		  
	}); 

});
</script>

<!-- 小視口全域導覽列 -->
<header class="patSmlHeader jsNavSml">
	<div class="patSmlHeader-navbt jsNavSml-bt" tabindex="-1" title="打開網站導覽列">
		<div class="patSmlHeader-navbt--icon patSmlHeader-navbt--icon01"></div>
		<div class="patSmlHeader-navbt--icon patSmlHeader-navbt--icon02"></div>
	</div>
</header>


<!-- 小視口全域導覽列開合區 -->
<div class="patSmlNav jsNavSmall">
	<!-- 全域導覽列連結 -->
	<div class="mt-50"></div>
	<ul class="patLevelArea jsAccording">
		<a href="javascript:void(0);" class="patLevelArea-firstLink jsAccording-firstLik" title="關於黑傑克" tabindex="-1" data-i18n="common_feature">
			⦿　關於黑傑克
		</a>
		<!-- 第二層 -->
		<ul class="patLevelArea-secondLinkArea jsAccording-secondArea">
		<a href="about.php" class="patLevelArea-secondLinkArea--link" title="寫在規劃保單之前：買保險真正的意義" tabindex="-1" data-i18n="">
				▸ 寫在規劃保單之前：買保險真正的意義
			</a>
			<a href="about.php" class="patLevelArea-secondLinkArea--link" title="加入黑傑克團隊" tabindex="-1" data-i18n="">
				▸ 保險黑傑克是誰？
			<a href="about.php" class="patLevelArea-secondLinkArea--link" title="加入黑傑克團隊" tabindex="-1" data-i18n="">
				▸ 什麼是「保險經紀人」
			</a>
			<a href="about.php" class="patLevelArea-secondLinkArea--link" title="加入黑傑克團隊" tabindex="-1" data-i18n="">
				▸ 加入黑傑克團隊
			</a>
		</ul>
		<div class="clear"></div>
	</ul>
	<ul class="patLevelArea jsAccording">
		<a href="javascript:void(0);" class="patLevelArea-firstLink jsAccording-firstLik" title="保險知識文章" tabindex="-1" data-i18n="common_feature">
			⦿　保險知識區
		</a>
		<!-- 第二層 -->
		<ul class="patLevelArea-secondLinkArea jsAccording-secondArea">
			<a href="articleli.php" class="patLevelArea-secondLinkArea--link" title="理賠案例" tabindex="-1" data-i18n="">
				▸ 理賠案例
			</a>
			<a href="articleli.php" class="patLevelArea-secondLinkArea--link" title="汽車保險" tabindex="-1" data-i18n="">
				▸ 汽車保險
			</a>
			<a href="articleli.php" class="patLevelArea-secondLinkArea--link" title="意外保險" tabindex="-1" data-i18n="">
				▸ 意外保險
			</a>
		</ul>
		<div class="clear"></div>
	</ul>
	<ul class="patLevelArea jsAccording">
		<a href="javascript:void(0);" class="patLevelArea-firstLink jsAccording-firstLik" title="常見問題區" tabindex="-1" data-i18n="common_feature">
			⦿　產險/團險/其他
		</a>
		<!-- 第二層 -->
		<ul class="patLevelArea-secondLinkArea jsAccording-secondArea">
			<a href="qali.php" class="patLevelArea-secondLinkArea--link" title="保險新手村" tabindex="-1" data-i18n="">
				▸ 保險新手村
			</a>
		</ul>
		<div class="clear"></div>
	</ul>
	<ul class="patLevelArea jsAccording">
		<a href="videoli.php" class="patLevelArea-firstLink jsAccording-firstLik" title="影音專區" tabindex="-1" data-i18n="common_feature">
			⦿　影音專區
		</a>
		<!-- 第二層 -->
		<!-- <ul class="patLevelArea-secondLinkArea jsAccording-secondArea">
			<a href="videoli.php" class="patLevelArea-secondLinkArea--link" title="保險新手村" tabindex="-1" data-i18n="">
				▸ 影片區
			</a>
		</ul> -->
		<div class="clear"></div>
	</ul>
	
</div>

<div class="patSmlNav-layer js-patSmlNav-layer"></div>