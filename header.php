<script language="javascript">
/*網站區js*/
$(document).ready(function() {
	// 大視口導覽列
	/*往上捲出現導覽列,往下隱藏*/
	let windowY = 0 ;
	const window500 = 500 ;
	$(window).scroll(function(){
		let nowY = window.pageYOffset ;
		// console.log("現在y:" + nowY);
		if (nowY > window500){
			// console.log("變換底色");
			$(".js-patheaderBk").addClass("patheaderBk--scrollStyle");
		}else {
			// console.log("去除底色");
			$(".js-patheaderBk").removeClass("patheaderBk--scrollStyle");
		}
	});

});
$(window).on('load',function(){
	ScrollTrigger.matchMedia({
		// ipad 
		"(min-width: 768px)": function() {
	
		},
        "(min-width: 1440px)": function() {
	
		},
	  
		// mobile
		"(max-width: 767px)": function() {

		},
		  
		// all 
		"all": function() {
			gsap.to(".js-patheaderBk", {
				delay: 1.2,
				duration: 0.5,
				opacity: "1",
				ease: "Power3.easeInOut",
			});	
		}		  
	}); 

});
</script>

<!-- 電腦版header -->
<header class="patheaderBk js-patheaderBk">
	<!-- <a href="index.php" class="patheaderBk-logo" title="">
		<img src="images/logo.svg" alt="LOGO" class="">
	</a> -->
	<ul class="patheaderLevelArea jsAccording">
		<a href="about.php" class="patheaderLevelArea-firstlink js-patheaderLevelArea-firstlink" title="關於黑傑克" tabindex="-1" data-i18n="common_feature">
			關於黑傑克
		</a>
		<div class="clear"></div>
	</ul>
	<ul class="patheaderLevelArea jsAccording">
		<a href="javascript:void(0);" class="patheaderLevelArea-firstlink js-patheaderLevelArea-firstlink" title="保險知識區" tabindex="-1" data-i18n="common_feature">
			保險知識區<img src="images/arrow-down-icon.svg" alt="" class="icon">
		</a>
		<!-- 第二層 -->
		<ul class="patheaderLevelArea-secondLinkArea jsAccording-secondArea">
			<a href="articleli.php" class="patheaderLevelArea-secondLinkArea--link" title="理賠案例" tabindex="-1" data-i18n="">
				<mark>▸</mark>理賠案例
			</a>
			<a href="articleli.php" class="patheaderLevelArea-secondLinkArea--link" title="汽車保險" tabindex="-1" data-i18n="">
				<mark>▸</mark>汽車保險
			</a>
			<a href="articleli.php" class="patheaderLevelArea-secondLinkArea--link" title="意外保險" tabindex="-1" data-i18n="">
				<mark>▸</mark>意外保險
			</a>
		</ul>
		<div class="clear"></div>
	</ul>
	<ul class="patheaderLevelArea jsAccording">
		<a href="javascript:void(0);" class="patheaderLevelArea-firstlink js-patheaderLevelArea-firstlink" title="常見問題區" tabindex="-1" data-i18n="common_feature">
			產險/團險/其他<img src="images/arrow-down-icon.svg" alt="" class="icon">
		</a>
		<!-- 第二層 -->
		<ul class="patheaderLevelArea-secondLinkArea jsAccording-secondArea">
			<a href="qali.php" class="patheaderLevelArea-secondLinkArea--link" title="理賠案例" tabindex="-1" data-i18n="">
				<mark>▸</mark>保險新手村
			</a>
		</ul>
		<div class="clear"></div>
	</ul>
	<ul class="patheaderLevelArea jsAccording">
		<a href="javascript:void(0);" class="patheaderLevelArea-firstlink js-patheaderLevelArea-firstlink" title="常見問題區" tabindex="-1" data-i18n="common_feature">
			影音專區<img src="images/arrow-down-icon.svg" alt="" class="icon">
		</a>
		<!-- 第二層 -->
		<ul class="patheaderLevelArea-secondLinkArea jsAccording-secondArea">
			<a href="videoli.php" class="patheaderLevelArea-secondLinkArea--link" title="理賠案例" tabindex="-1" data-i18n="">
				<mark>▸</mark>理賠案例
			</a>
		</ul>
		<div class="clear"></div>
	</ul>
	
	
</header>
